<?php

function jwl_zoom_settings_form() {

  $form = array();

	$form['jwl_zoom_fancy'] = array(
		'#type' => 'radios',
		'#title' => t('Fancy Settings'),
		'#default_value' => variable_get('jwl_zoom_fancy', FALSE),
    '#options' => array(TRUE => 'ON', FALSE => 'OFF'),
		'#required' => TRUE
	);

	return system_settings_form($form);
}
