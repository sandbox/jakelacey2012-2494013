<style>
.large {
	background: url(<?php print $zoom_style_path; ?>) no-repeat;
}

.large.blur {
	width: 230px;
	height: 230px;
	-webkit-filter: blur(20px);
  filter: url(<?php print $zoom_style_path; ?>);
  filter: blur(20px);
}

.large.inner {
	width: 205px;
	height: 205px;
	margin: 12px;
}
</style>
<div class="magnify">
  <div class="large"></div>
	<?php if ($fancyEffects == TRUE) : ?>
		<div class="large blur"></div>
		<div class="large inner"></div>
	<?php endif; ?>
  <img class="small" src="<?php print $normal_style_path; ?>" >
</div>
